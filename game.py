#!/bin/bash

import sys
import pandas as pd
from src.preprocessing import Preprocessor
from src.utils import util_functions as futils
from src.relation_detection import modelling
from src.entity_detection import entity_detection
from src.training import training
from src.entity_linking import entity_linking
import random
import time
#TODO: Correct the questions with different correct answers
#TODO: Put AI's answer in the selections
sys.path.append('/data')
sys.path.append('/models')
sys.path.append('/objects')

sequencer = futils.unpickle_data(path='objects', filename='sequencer.pkl')
padder = futils.unpickle_data(path='objects', filename='padder.pkl')
encoder = futils.unpickle_data(path='objects', filename='encoder.pkl')
rel_model = 'rel_model_LSTM_1_50'
ent_model = 'ent_model_GRU_1_50'
relation_detector = training.read_model(f'models/{rel_model}.h5')
entity_detector = training.read_model(f'models/{ent_model}.h5')
word_index = sequencer.word_index

time.sleep(90)

for i in range(5):
    correct_question = entity_linking.get_question()
    question = correct_question[0]['_source']['question']
    category = correct_question[0]['_source']['label']
    obj = correct_question[0]['_source']['obj_descr']

    print(question)
    print(obj)
    print(category)

    questions = entity_linking.get_other_options(category=category, obj_descr=obj)
    questions.extend(correct_question)

    answer_list = [q['_source']['obj_descr'] for q in questions]
    answer_list = random.sample(answer_list, 4)

    print(question, '\n')
    choices = ['A', 'B', 'C', 'D']
    for choice, answer in zip(choices, answer_list):
        print(f'{choice}: {answer}')

    user_choice = input("Your Choice:")
    ground_truth = entity_linking.get_ground_truth(question, category=category)

    question = Preprocessor.clean_text(pd.Series([question]))
    sequence = sequencer.transform(question)
    sequence = padder.transform(sequence)
    relation_prediction = modelling.predict_model(relation_detector, sequence, encoder)
    entity_prediction = entity_detection.predict_model(entity_detector,
                                                       sequence,
                                                       word_index, question,
                                                       cutoff=0.5)

    ai_answer = entity_linking.get_answers(relation_prediction[0], entity_prediction[0])['obj_descr']
    print(ai_answer, relation_prediction, entity_prediction)
    choice_index = answer_list.index(ground_truth)
    correct_choice = choices[choice_index]

    if ai_answer in answer_list:
        ai_choice_index = answer_list.index(ai_answer)
        ai_choice = choices[ai_choice_index]
    else:
        ai_choice = random.sample(choices, 1)[0]
        ai_choice_index = choices.index(ai_choice)
        ai_answer = answer_list[ai_choice_index]
    user_choice_index = choices.index(user_choice)
    user_answer = answer_list[user_choice_index]

    if ai_choice == correct_choice:
        print(f"Correct, AI predicts {ai_choice}, {ai_answer}")
    else:
        print(f"Wrong, AI predicts {ai_choice}, {ai_answer}")
    if user_choice == correct_choice:
        print(f"Correct, user predicts {user_choice}, {user_answer}")
    else:
        print(f"Wrong, user predicts {user_choice}, {user_answer}")
    if ai_choice != correct_choice and user_choice != correct_choice:
        print(f'Correct Answer: {correct_choice}, {ground_truth}')

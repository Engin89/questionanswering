import sys
import pandas as pd
import numpy as np
from src.preprocessing import Preprocessor
from src.utils import util_functions as futils
from src.relation_detection import modelling
from src.entity_detection import entity_detection
from src.training import training
from src.indexing import create_index
from src.entity_linking import entity_linking
from src.answer_evaluation import answer_evaluation

sys.path.append('../data')
sys.path.append('../models')

train = futils.load_data('../data/annotated_fb_data_train.txt', ['subject', 'relation', 'object', 'question'])
test = futils.load_data('../data/annotated_fb_data_test.txt', ['subject', 'relation', 'object', 'question'])
validation = futils.load_data('../data/annotated_fb_data_valid.txt', ['subject', 'relation', 'object', 'question'])
mappings = futils.load_data('../data/freebase-rs-label-en.dms', ['mid', 'description'], [0, 2])

mappings['mid'] = mappings['mid'].apply(lambda x: futils.replace_subject_mappings(x))

train = futils.merge_data(train, mappings)
test = futils.merge_data(test, mappings)
validation = futils.merge_data(validation, mappings)
futils.delete_data(mappings)

train['description'] = Preprocessor.clean_text(train['description'])
test['description'] = Preprocessor.clean_text(test['description'])
validation['description'] = Preprocessor.clean_text(validation['description'])

train['question'] = Preprocessor.clean_text(train['question'])
test['question'] = Preprocessor.clean_text(test['question'])
validation['question'] = Preprocessor.clean_text(validation['question'])

entity_preprocessor = Preprocessor.EntityPreprocessor()
train_targets = entity_preprocessor.create_targets(train)
test_targets = entity_preprocessor.create_targets(test)
validation_targets = entity_preprocessor.create_targets(validation)

[*map(entity_preprocessor.zerolistmaker, train_targets)]
[*map(entity_preprocessor.zerolistmaker, validation_targets)]
[*map(entity_preprocessor.zerolistmaker, test_targets)]

sequencer = Preprocessor.Sequencer(oov_token='<OOV>', filters='!"#$%&()+,./*:;<=>?...')
sequencer.fit(pd.concat([train['question'], validation['question'], test['question']], ignore_index=True))
word_index = sequencer.word_index

train_sequences = sequencer.transform(train['question'])
valid_sequences = sequencer.transform(validation['question'])
test_sequences = sequencer.transform(test['question'])

padder = Preprocessor.Padder()
X_train = padder.transform(train_sequences)
X_valid = padder.transform(valid_sequences)
X_test = padder.transform(test_sequences)

# Relation Detection target pre-processing
train_relation = train.relation
valid_relation = validation.relation
test_relation = test.relation
relation_list = [test_relation, valid_relation, train_relation]
encoder = Preprocessor.relation_fit(relation_list)
Y_train = Preprocessor.relation_transform(train_relation, encoder)
Y_test = Preprocessor.relation_transform(test_relation, encoder)
Y_valid = Preprocessor.relation_transform(valid_relation, encoder)

layer_types = ['LSTM', 'GRU']
bidirection_inds = [1, 0]
dropouts = [0, 25, 50]
path = "../models"

# TODO: Make it functional
embedder = modelling.Embedder(word_index=word_index)
embedding_layer = embedder.specify_embedding_layer()

import tensorflow as tf
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Input, Dense, Embedding, Dropout, LSTM, Bidirectional, GRU


def joint_nn(input_shape, embedding_layer):  # (input_shape, word_to_vec_map, word_to_index):
    sentence_indices = Input(shape=input_shape, dtype='int32')
    embedding_layer = embedding_layer  # pretrained_embedding_layer(word_to_vec_map, word_to_index)
    embeddings = embedding_layer(sentence_indices)
    X = Bidirectional(GRU(300, return_sequences=True))(embeddings)
    X = Bidirectional(GRU(300, return_sequences=False))(X)
    X = Dropout(rate=0.5)(X)
    rel_output_layer = Dense(1837, activation='softmax', name='rel_out')(X)
    ent_output_layer = Dense(34, activation='sigmoid', name='ent_out')(X)
    joint_model = Model(inputs=sentence_indices, outputs=[rel_output_layer, ent_output_layer])
    return joint_model


joint_model = joint_nn(input_shape=(None,), embedding_layer=embedding_layer)
opt = tf.keras.optimizers.Adam(learning_rate=0.0001)
joint_model.compile(loss={'rel_out': 'categorical_crossentropy',
                          'ent_out': 'binary_crossentropy'},
                    loss_weights={'rel_out':1,'ent_out': 5},
                    optimizer=opt,
                    metrics=['accuracy', training.recall_m, training.precision_m, training.f1_m])

history = joint_model.fit(X_train, {'rel_out': Y_train, 'ent_out': np.array(train_targets)},
                          epochs=19, batch_size=32, validation_data=(X_valid,
                                                                      {"rel_out": Y_valid,
                                                                       "ent_out": np.array(validation_targets)}))

rel_predictions, ent_predictions = joint_model.predict(X_test)
rel_predictions = modelling._convert_to_relations(rel_predictions, encoder)


def convert_to_words(predictions, source, word_index, question):
    reverse_word_map = dict(map(reversed, word_index.items()))
    l1 = []
    for (pred, x, q) in zip(predictions, source, question):
        pred = pred[:len(q.split())]
        x = x[:len(q.split())]
        pos = np.where(pred >= 0.5)[0]
        l2 = []
        for p in x[pos]:
            l2.append(reverse_word_map.get(p))
        try:
            l2 = ' '.join(l2)
        except TypeError:
            l2 = ' '
        l1.append(l2)
    return l1


ent_predictions = convert_to_words(ent_predictions, X_test, word_index, test.question)
rel_preds_2 = []
ent_preds_2 = []
for ent, rel in zip(ent_predictions, rel_predictions):
    rel_preds_2.append(entity_linking.get_results(rel, ent)['relation'])
    ent_preds_2.append(entity_linking.get_results(rel, ent)['subject'])

def evaluate_answers(subjects, relations, eval_df, scoring):
    predictions_df = pd.DataFrame(zip(subjects, relations), columns=['subject', 'relation'])
    accuracy = ((predictions_df['subject'] == eval_df['subject']) & (
                predictions_df['relation'] == eval_df['relation'])).sum() / eval_df.shape[0]
    print("accuracy with {0}: {1:.4f}".format(scoring, accuracy))

evaluate_answers(subjects=ent_preds_2, relations=rel_preds_2, eval_df=test, scoring='BM25')
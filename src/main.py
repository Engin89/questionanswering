import sys
import pandas as pd
import numpy as np
from src.preprocessing import Preprocessor
from src.utils import util_functions as futils
from src.relation_detection import modelling
from src.entity_detection import entity_detection
from src.training import training
from src.indexing import create_index
from src.entity_linking import entity_linking
from src.answer_evaluation import answer_evaluation

sys.path.append('../data')
sys.path.append('../models')

train = futils.load_data('../data/annotated_fb_data_train.txt', ['subject', 'relation', 'object', 'question'])
test = futils.load_data('../data/annotated_fb_data_test.txt', ['subject', 'relation', 'object', 'question'])
validation = futils.load_data('../data/annotated_fb_data_valid.txt', ['subject', 'relation', 'object', 'question'])
mappings = futils.load_data('../data/freebase-rs-label-en', ['mid', 'description'], [0, 2])

mappings['mid'] = mappings['mid'].apply(lambda x: futils.replace_subject_mappings(x))

train = futils.merge_data(train, mappings)
test = futils.merge_data(test, mappings)
validation = futils.merge_data(validation, mappings)
futils.delete_data(mappings)

train['description'] = Preprocessor.clean_text(train['description'])
test['description'] = Preprocessor.clean_text(test['description'])
validation['description'] = Preprocessor.clean_text(validation['description'])

train['question'] = Preprocessor.clean_text(train['question'])
test['question'] = Preprocessor.clean_text(test['question'])
validation['question'] = Preprocessor.clean_text(validation['question'])

entity_preprocessor = Preprocessor.EntityPreprocessor()
train_targets = entity_preprocessor.create_targets(train)
test_targets = entity_preprocessor.create_targets(test)
validation_targets = entity_preprocessor.create_targets(validation)

[*map(entity_preprocessor.zerolistmaker, train_targets)]
[*map(entity_preprocessor.zerolistmaker, validation_targets)]
[*map(entity_preprocessor.zerolistmaker, test_targets)]

sequencer = Preprocessor.Sequencer(oov_token='<OOV>', filters='!"#$%&()+,./*:;<=>?...')
sequencer.fit(pd.concat([train['question'], validation['question'], test['question']], ignore_index=True))
word_index = sequencer.word_index

train_sequences = sequencer.transform(train['question'])
valid_sequences = sequencer.transform(validation['question'])
test_sequences = sequencer.transform(test['question'])

padder = Preprocessor.Padder()
X_train = padder.transform(train_sequences)
X_valid = padder.transform(valid_sequences)
X_test = padder.transform(test_sequences)

# Relation Detection target pre-processing
train_relation = train.relation
valid_relation = validation.relation
test_relation = test.relation
relation_list = [test_relation, valid_relation, train_relation]
encoder = Preprocessor.relation_fit(relation_list)
Y_train = Preprocessor.relation_transform(train_relation, encoder)
Y_test = Preprocessor.relation_transform(test_relation, encoder)
Y_valid = Preprocessor.relation_transform(valid_relation, encoder)

layer_types = ['LSTM', 'GRU']
bidirection_inds = [1, 0]
dropouts = [0, 25, 50]
path = "../models"
for layer_type in layer_types:
    for bidirection in bidirection_inds:
        for dropout in dropouts:
            rel_model_name = f"rel_model_{layer_type}_{str(bidirection)}_{str(dropout)}.h5"
            rel_hist = f"rel_model_{layer_type}_{str(bidirection)}_{str(dropout)}"
            ent_model_name = f"ent_model_{layer_type}_{str(bidirection)}_{str(dropout)}.h5"
            ent_hist = f"ent_model_{layer_type}_{str(bidirection)}_{str(dropout)}"
            print(f"iteration for {rel_model_name} and {ent_model_name} is starting")
            history, relation_detector = training.train_model(X_train=X_train,
                                                              Y_train=Y_train,
                                                              X_valid=X_valid,
                                                              Y_valid=Y_valid,
                                                              word_index=word_index,
                                                              layer_type=layer_type,
                                                              bidirection = bidirection,
                                                              dropout= dropout,
                                                              epochs=8,
                                                              batch_size=256,
                                                              shuffle=True,
                                                              train_function=modelling.fit_model)
            training.save_model(relation_detector, path=path, model_name=rel_model_name)
            training.save_history(history, path=path, model_name=rel_hist)

            print('relation_detection evaluating:')
            training.evaluate_model(relation_detector, X_test, Y_test)

            history, entity_detector = training.train_model(X_train=X_train,
                                                            Y_train=train_targets,
                                                            X_valid=X_valid,
                                                            Y_valid=validation_targets,
                                                            word_index=word_index,
                                                            layer_type=layer_type,
                                                            bidirection=bidirection,
                                                            dropout=dropout,
                                                            epochs=13,
                                                            batch_size=256,
                                                            shuffle=True,
                                                            train_function=entity_detection.fit_model)

            training.save_model(entity_detector,path=path, model_name=ent_model_name)
            training.save_history(history, path=path, model_name=ent_hist)
            print('entity_detection evaluating:')
            training.evaluate_model(entity_detector, X_test, test_targets)


relation_detector = training.read_model('../models/relation_detection_model.h5')
entity_detector = training.read_model('../models/entity_detection_model.h5')

relation_predictions = modelling.predict_model(relation_detector, X_valid, encoder)
entity_predictions = entity_detection.predict_model(entity_detector, X_valid, word_index, validation.question,
                                                    cutoff=0.4)

entities = list(np.concatenate([train[['subject', 'description']].drop_duplicates().description.values,
                                validation[['subject', 'description']].drop_duplicates().description.values,
                                test[['subject', 'description']].drop_duplicates().description.values]))

entities_mid = list(np.concatenate([train[['subject', 'description']].drop_duplicates().subject.values,
                                    validation[['subject', 'description']].drop_duplicates().subject.values,
                                    test[['subject', 'description']].drop_duplicates().subject.values]))

inverse_index = create_index.create_inverse_index(entities, entities_mid)
reach_index = create_index.create_reach_index([train, validation, test])
main_index = create_index.create_main_index([train, validation, test], cols=['subject',
                                                                             'description',
                                                                             'relation'])
create_index.post_to_es(index=main_index, index_name='main_index', field_limits=1000)

validation = futils.create_column(df=validation,
                                  params=[entity_predictions, relation_predictions],
                                  colnames=['entity_predictions', 'relation_predictions'])

# create predictions for entities and relations based on levenshtein distance
ent_preds, rel_preds = entity_linking.get_predictions(validation, reach_index, inverse_index)

# create predictions for entities and relations based on TFIDF (BM25) via Elasticsearch
rel_preds_2 = []
ent_preds_2 = []
for ent, rel in zip(entity_predictions, relation_predictions):
    rel_preds_2.append(entity_linking.get_results(rel, ent)['relation'])
    ent_preds_2.append(entity_linking.get_results(rel, ent)['subject'])

ents = [ent_preds, ent_preds_2]
rels = [rel_preds, rel_preds_2]
scoring = ['levensthein', 'BM25']

for ent, rel, score in zip(ents, rels, scoring):
    answer_evaluation.evaluate_answers(subjects=ent, relations=rel, eval_df=validation, scoring=score)

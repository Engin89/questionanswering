import tensorflow as tf
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Input, Dense, Embedding, Dropout, LSTM, Bidirectional, GRU
from tensorflow.keras.callbacks import EarlyStopping
from src.training import training
# from keras.callbacks import TensorBoard
import numpy as np
import sys

sys.path.append('../models')


class Embedder:
    def __init__(self,
                 word_index,
                 dimension=300,
                 input_length=34,
                 path='../data/glove.6B.300d.txt'):
        self.dimension = dimension
        self.input_length = input_length
        self.path = path
        self.word_index = word_index

    def _load_embeddings(self):
        embedding = {}
        with open(self.path, encoding="utf8") as f:
            for line in f:
                vectors = line.split()
                word = vectors[0]
                embedding[word] = vectors[1:]
        embedding_matrix = np.zeros((len(self.word_index) + 1, self.dimension))
        for word, i in self.word_index.items():
            embedding_vector = embedding.get(word)
            if embedding_vector is not None:
                embedding_matrix[i] = embedding_vector
        return embedding_matrix

    def specify_embedding_layer(self):
        embedding_matrix = self._load_embeddings()
        embedding_layer = Embedding(len(self.word_index) + 1,
                                    self.dimension,
                                    weights=[embedding_matrix],
                                    input_length=self.input_length,
                                    trainable=False)
        return embedding_layer


def LSTM_relation_model(input_shape, embedding_layer, layer_type, bidirection, dropout):
    sentence_indices = Input(shape=input_shape, dtype='int32')
    embedding_layer = embedding_layer
    embeddings = embedding_layer(sentence_indices)
    if layer_type == 'LSTM':
        if bidirection == 1:
            X = Bidirectional(LSTM(300, return_sequences=True))(embeddings)
            X = Bidirectional(LSTM(300, return_sequences=False))(X)
        else:
            X = LSTM(300, return_sequences=True)(embeddings)
            X = LSTM(300, return_sequences=False)(X)
    else:
        if bidirection == 1:
            X = Bidirectional(GRU(300, return_sequences=True))(embeddings)
            X = Bidirectional(GRU(300, return_sequences=False))(X)
        else:
            X = GRU(300, return_sequences=True)(embeddings)
            X = GRU(300, return_sequences=False)(X)
    if dropout != 0:
        X = Dropout(rate=dropout/100)(X)
    # X = Dense(600, activation='relu')(X)
    X = Dense(1837, activation='softmax')(X)
    model = Model(inputs=sentence_indices, outputs=X)
    return model


def get_summary(word_index, layer_type, bidirection, dropout):
    tf.compat.v1.reset_default_graph()
    embedder = Embedder(word_index=word_index)
    model = LSTM_relation_model((None,), embedding_layer=embedder.specify_embedding_layer(),
                                layer_type = layer_type, bidirection = bidirection, dropout = dropout)
    model.summary()
    return model


def compile_model(word_index,
                  layer_type,
                  bidirection,
                  dropout,
                  # log_dir="logs/fit/relation_model/",
                  mode='min',
                  patience=2,
                  monitor='val_loss',
                  loss='categorical_crossentropy',
                  optimizer='adam',
                  metrics=None):
    if metrics is None:
        metrics = ['accuracy', training.recall_m, training.precision_m, training.f1_m]
    model = get_summary(word_index=word_index, layer_type = layer_type, bidirection = bidirection, dropout = dropout)
    # log_dir = log_dir
    # tensorboard_callback = TensorBoard(log_dir=log_dir, histogram_freq=1, profile_batch=100000000)
    callback = EarlyStopping(monitor=monitor, patience=patience, mode=mode)
    model.compile(loss=loss, optimizer=optimizer, metrics=metrics)
    return model, callback


def fit_model(X_train,
              Y_train,
              X_valid,
              Y_valid,
              word_index,
              layer_type,
              bidirection,
              dropout,
              epochs=8,
              batch_size=256,
              shuffle=True):
    validation_data = (X_valid, Y_valid)
    model, callback = compile_model(word_index=word_index,
                                    layer_type = layer_type,
                                    bidirection = bidirection,
                                    dropout =dropout)
    history = model.fit(X_train, Y_train, epochs=epochs, batch_size=batch_size, validation_data=validation_data,
                        shuffle=shuffle,
                        callbacks=[callback])
    return history, model


def predict_model(model, X, encoder):
    predictions = model.predict(X)
    predictions = _convert_to_relations(predictions, encoder)
    return predictions


def _convert_to_relations(predictions, encoder):
    predictions = np.argmax(predictions, axis=1)
    predictions = encoder.inverse_transform(predictions)
    return predictions


from nltk.util import ngrams
import re
import nltk
from elasticsearch import Elasticsearch
import requests
import random

nltk.download('stopwords')
stopwords = nltk.corpus.stopwords.words("english")


def get_candidate_entities(entity_predictions, relation_predictions, inverse_index):
    pred_case_list = []
    string_list = []
    candidate_nodes = []
    candidate_levenshtein = []
    n = len(entity_predictions.split())
    if n > 2 and entity_predictions.split()[0] in stopwords:
        entity_predictions = ' '.join(entity_predictions.split()[1:])
        n = len(entity_predictions.split())
    entity_predictions = re.sub(r'[^\w\s]', '', entity_predictions)

    # calculate n-grams per subject entity
    for i in range(n + 1, 0, -1):
        pred_case_list.extend(list(ngrams(entity_predictions.split(), i)))

    for w in pred_case_list:
        string = ' '.join(w)
        string_list.append(string)

    for s in string_list:
        if s not in stopwords:
            if inverse_index.get(s) is None:
                pass
            else:
                candidate_nodes.extend(inverse_index[s]['node'])
                candidate_levenshtein.extend(inverse_index[s]['levenshtein'])

                if len(s.split()) <= n and len(candidate_nodes) > 0 and len(s.split()) != 1:
                    break
        else:
            pass
    cand_tuple = zip(candidate_nodes, candidate_levenshtein)
    cand_dict = {i[0]: {'relation': relation_predictions, 'levenshtein': i[1]} for i in cand_tuple}
    return cand_dict


def get_predictions(df, reach_index, inverse_index):
    subjects_mid_pred = []
    relation_pred = []
    for i, j in df.iterrows():
        temp_key = []
        temp_relation = []
        temp_distance = []
        candidates = get_candidate_entities(j['entity_predictions'], j['relation_predictions'], inverse_index)
        for k, v in candidates.items():
            if v['relation'] in reach_index[k]:
                temp_key.append(k)
                temp_relation.append(v['relation'])
                temp_distance.append(v['levenshtein'])
        chained_list = [*zip(temp_key, temp_relation, temp_distance)]
        try:
            subjects_mid_pred.append(sorted(chained_list, key=lambda x: x[2], reverse=True)[0][0])
            relation_pred.append(sorted(chained_list, key=lambda x: x[2], reverse=True)[0][1])
        except IndexError:
            subjects_mid_pred.append(k)
            relation_pred.append(v['relation'])
    return subjects_mid_pred, relation_pred


def get_results(relation_predictions, entity_predictions):
    request_body = {
        "query": {
            "bool": {
                "must": [
                    {"match": {"description": entity_predictions}}
                ],
                "filter": [
                    {"term": {"relation.keyword": relation_predictions}}
                ]
            }
        }
    }
    es = Elasticsearch(hosts=[{"local": '9200'}])
    try:
        res = es.search(index="main_index",
                        body=request_body,
                        size=1)['hits']['hits'][0]['_source']
        return res
    except IndexError:
        d = {'subject': 'missing',
             'description': entity_predictions,
             'relation': relation_predictions}
        return d


def get_answers(relation_predictions, entity_predictions):
    request_body = {
        "query": {
            "bool": {
                "must": [
                    {"match": {"description": entity_predictions}}
                ],
                "filter": [
                    {"term": {"relation.keyword": relation_predictions}}
                ]
            }
        }
    }
    es = Elasticsearch(hosts=[{"local": '9200'}])
    try:
        res = es.search(index="answer_index",
                        body=request_body,
                        size=1)['hits']['hits'][0]['_source']
        return res
    except IndexError:
        d = {'subject': 'missing',
             'description': entity_predictions,
             'relation': relation_predictions,
             'obj_descr': 'NotFound'}
        return d


def get_question():
    categories = ['NO NER', 'GPE', 'PERSON', 'DATE', 'LOC', 'ORG', 'NORP', 'MONEY',
                  'CARDINAL', 'FAC', 'TIME', 'EVENT', 'LANGUAGE', 'PRODUCT',
                  'ORDINAL', 'LAW', 'QUANTITY', 'WORK_OF_ART']
    category = random.choice(categories)

    request_body = {
        "query": {
            "bool": {
                "must": [
                    {
                        "function_score": {
                            "functions": [
                                {
                                    "random_score": {
                                    }
                                }
                            ]
                        }
                    }
                ], "filter": [
                    {"term": {"label.keyword": category}}
                ]
            }
        }
    }
    es = Elasticsearch(hosts=[{"local": '9200'}])
    res = es.search(index="question_index",
                    body=request_body,
                    size=1)['hits']['hits']  # [0]['_source']
    return res


def get_other_options(category, obj_descr):
    request_body = {
        "query": {
            "bool": {
                "must_not": [
                    {
                        "match": {
                            "obj_descr": obj_descr
                        }
                    }
                ],
                "must": [
                    {
                        "function_score": {
                            "functions": [
                                {
                                    "random_score": {
                                    }
                                }
                            ]
                        }
                    }
                ],
                "filter": [
                    {"term": {"label.keyword": category}}
                ]
            }
        }
    }
    es = Elasticsearch(hosts=[{"local": '9200'}])
    res = es.search(index="question_index",
                    body=request_body,
                    size=3)['hits']['hits']
    return res


def get_ground_truth(question, category):
    question = re.sub(r'"', '', question)
    request_body = {
        "query": {
            "bool": {
                "must": [
                    {"match": {"question": question}}
                ],
                "filter": [
                    {"term": {"label.keyword": category}}
                ]
            }
        }
    }
    es = Elasticsearch(hosts=[{"local": '9200'}])
    res = es.search(index="question_index",
                    body=request_body,
                    size=1)['hits']['hits'][0]['_source']['obj_descr']
    return res

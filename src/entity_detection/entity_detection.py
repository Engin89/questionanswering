import tensorflow as tf
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Input, Dense, Embedding, Dropout, GRU, Bidirectional, LSTM
from tensorflow.keras.callbacks import EarlyStopping
# from keras.callbacks import TensorBoard
from tensorflow.keras.optimizers import Adam
from src.training import training
import numpy as np
import sys

sys.path.append('../models')


class Embedder:
    def __init__(self,
                 word_index,
                 dimension=300,
                 input_length=34,
                 path='../data/glove.6B.300d.txt'):
        self.dimension = dimension
        self.input_length = input_length
        self.path = path
        self.word_index = word_index

    def _load_embeddings(self):
        embedding = {}
        with open(self.path, encoding="utf8") as f:
            for line in f:
                vectors = line.split()
                word = vectors[0]
                embedding[word] = vectors[1:]
        embedding_matrix = np.zeros((len(self.word_index) + 1, self.dimension))
        for word, i in self.word_index.items():
            embedding_vector = embedding.get(word)
            if embedding_vector is not None:
                embedding_matrix[i] = embedding_vector
        return embedding_matrix

    def specify_embedding_layer(self):
        embedding_matrix = self._load_embeddings()
        embedding_layer = Embedding(len(self.word_index) + 1,
                                    self.dimension,
                                    weights=[embedding_matrix],
                                    input_length=self.input_length,
                                    trainable=False)
        return embedding_layer


def entity_detection_model(input_shape, embedding_layer, layer_type, bidirection, dropout):
    sentence_indices = Input(shape=input_shape, dtype='int32')
    embedding_layer = embedding_layer
    embeddings = embedding_layer(sentence_indices)
    if layer_type == "LSTM":
        if bidirection == 1:
            X = Bidirectional(LSTM(300, return_sequences=True))(embeddings)
            X = Bidirectional(LSTM(300, return_sequences=False))(X)
        else:
            X = LSTM(300, return_sequences=True)(embeddings)
            X = LSTM(300, return_sequences=False)(X)
    else:
        if bidirection == 1:
            X = Bidirectional(GRU(300, return_sequences=True))(embeddings)
            X = Bidirectional(GRU(300, return_sequences=False))(X)
        else:
            X = GRU(300, return_sequences=True)(embeddings)
            X = GRU(300, return_sequences=False)(X)
    if dropout != 0:
        X = Dropout(rate=dropout/ 100)(X)
    X = Dense(34, activation='sigmoid')(X)
    model = Model(inputs=sentence_indices, outputs=X)
    return model


def get_summary(word_index, layer_type, bidirection, dropout):
    tf.compat.v1.reset_default_graph()
    embedder = Embedder(word_index=word_index)
    model = entity_detection_model((None,), embedding_layer=embedder.specify_embedding_layer(),
                                   layer_type = layer_type, bidirection = bidirection, dropout = dropout)
    model.summary()
    return model


def compile_model(word_index,
                  layer_type,
                  bidirection,
                  dropout,
                  # log_dir="logs/fit/relation_model/",
                  mode='min',
                  patience=3,
                  monitor='val_f1_m',
                  loss='binary_crossentropy',
                  optimizer=None,
                  learning_rate=0.0001,
                  metrics=None):
    if metrics is None:
        metrics = ['accuracy', training.recall_m, training.precision_m, training.f1_m]
    if optimizer is None:
        optimizer = Adam(learning_rate=learning_rate, beta_1=0.9, beta_2=0.999, epsilon=1e-07, amsgrad=False)
    model = get_summary(word_index=word_index, layer_type = layer_type, bidirection = bidirection, dropout = dropout)
    # log_dir = log_dir
    # tensorboard_callback = TensorBoard(log_dir=log_dir, histogram_freq=1, profile_batch=100000000)
    callback = EarlyStopping(monitor=monitor, patience=patience, mode=mode)
    model.compile(loss=loss, optimizer=optimizer, metrics=metrics)
    return model, callback


def fit_model(X_train,
              Y_train,
              X_valid,
              Y_valid,
              word_index,
              layer_type,
              bidirection,
              dropout,
              epochs=13, batch_size=256, shuffle=True):
    validation_data = (X_valid, np.array(Y_valid))
    model, callback = compile_model(word_index=word_index,
                                    layer_type = layer_type,
                                    bidirection = bidirection,
                                    dropout = dropout)
    history = model.fit(X_train,
                        np.array(Y_train),
                        epochs=epochs,
                        batch_size=batch_size,
                        validation_data=validation_data,
                        shuffle=shuffle)#,
                       # callbacks=[callback])
    return history, model


def predict_model(model, X, word_index, question, cutoff=0.4):
    predictions = model.predict(X)
    temp_1 = np.zeros(predictions.shape)
    for i in range(temp_1.shape[0]):
        if np.max(predictions[i]) >= cutoff:
            temp_1[i] = np.where(predictions[i] >= cutoff, 1, 0)
        else:
            temp_1[i] = np.where(predictions[i] >= sorted(predictions[i])[-2], 1, 0)
    entities = _convert_to_words(temp_1, X, word_index, question)
    return entities


def _convert_to_words(predictions, source, word_index, question):
    reverse_word_map = dict(map(reversed, word_index.items()))
    l1 = []
    for (pred, x, q) in zip(predictions, source, question):
        pred = pred[:len(q.split())]
        x = x[:len(q.split())]
        pos = np.where(pred == 1)[0]
        l2 = []
        for p in x[pos]:
            l2.append(reverse_word_map.get(p))
        try:
            l2 = ' '.join(l2)
        except TypeError:
            l2 = ' '
        l1.append(l2)
    return l1



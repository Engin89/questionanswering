import pandas as pd
from fuzzywuzzy import fuzz
from nltk.util import ngrams
from elasticsearch import Elasticsearch
from elasticsearch.helpers import bulk
import json
import re


def create_reach_index(df_list):
    reach_index = {}
    for i, j in pd.concat(df_list).iterrows():
        if reach_index.get(j['subject']) is not None:
            reach_index[j['subject']].extend([j['relation']])
        else:
            reach_index[j['subject']] = [j['relation']]
    return reach_index


def create_inverse_index(entities, entities_mid):
    inverse_index = {}
    for ent, mid in zip(entities, entities_mid):
        n = len(ent.split())
        test_case_list = []
        # calculate n-grams per subject entity
        for i in range(1, n + 1):
            test_case_list.extend(list(ngrams(ent.split(), i)))

            # convert n-grams tuples to list
        for w in test_case_list:
            string = " ".join(w)
            if string != ' ':
                if inverse_index.get(str(re.sub(r"[^\w\s]", "", string))) is None:
                    inverse_index[re.sub(r"[^\w\s]", "", string)] = {"node": [mid],
                                                                     "levenshtein": [
                                                                         fuzz.ratio(re.sub(r"[^\w\s]", "", ent),
                                                                                    re.sub(r"[^\w\s]", "",
                                                                                           string))]}
                elif mid in inverse_index[re.sub(r'[^\w\s]', '', string)]["node"]:
                    pass
                else:
                    inverse_index[re.sub(r"[^\w\s]", "", string)]["node"].append(mid)
                    inverse_index[re.sub(r"[^\w\s]", "", string)]["levenshtein"].append(
                        fuzz.ratio(re.sub(r"[^\w\s]", "", ent), re.sub(r"[^\w\s]", "", string)))
    return inverse_index


def create_main_index(df, cols):
    df_all = pd.concat(df)[cols].drop_duplicates()
    return df_all.to_json(orient= 'records', force_ascii = False)


def post_to_es(index, index_name, field_limits=1000):
    request_body = {
        "settings": {
            "index.mapping.total_fields.limit":field_limits
        }
    }

    es = Elasticsearch(hosts=[{"local": '9200'}])
    if not es.indices.exists(index=index_name):
        es.indices.create(index=index_name, body = request_body)
        test_index = json.loads(index)
        actions = []
        for i, line in enumerate(test_index, 1):
            d = {
                '_index': index_name,
                '_type': 'application/json',
                '_id': i,
                '_source': {k: v for k, v in line.items()}
            }
            actions.append(d)
        bulk(es, actions, request_timeout=10, chunk_size=500)
    else:
        pass




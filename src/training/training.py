from tensorflow.keras import backend as K
from tensorflow.keras.models import load_model
import numpy as np
import pickle
import os


def train_model(X_train, Y_train, X_valid, Y_valid, word_index, epochs, batch_size, shuffle, train_function, layer_type, bidirection, dropout):
    history, model = train_function(X_train,
                           Y_train,
                           X_valid,
                           Y_valid,
                           layer_type = layer_type,
                           bidirection = bidirection,
                           dropout = dropout,
                           word_index=word_index,
                           epochs=epochs,
                           batch_size=batch_size,
                           shuffle=shuffle)
    return history, model


def save_model(model, model_name, path):
    model.save(os.path.join(path, model_name))


def save_history(history, model_name, path):
    with open(os.path.join(path, model_name), 'wb') as file_pi:
        pickle.dump(history.history, file_pi)


def evaluate_model(model, X, Y):
    loss, acc, recall, precision, f1 = model.evaluate(X, np.array(Y))
    print("loss: {0:.2f}, accuracy: {1:.2f}, recall: {2:.2f}, precision: {3:.2f}, f1 score:{4:.2f}".
          format(loss, acc, recall, precision, f1))


def read_model(path):
    model = load_model(path, custom_objects={"recall_m": recall_m, "precision_m": precision_m, "f1_m": f1_m})
    return model


def recall_m(y_true, y_pred):
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
    recall = true_positives / (possible_positives + K.epsilon())
    return recall


def precision_m(y_true, y_pred):
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
    precision = true_positives / (predicted_positives + K.epsilon())
    return precision


def f1_m(y_true, y_pred):
    precision = precision_m(y_true, y_pred)
    recall = recall_m(y_true, y_pred)
    return 2 * ((precision * recall) / (precision + recall + K.epsilon()))
import pandas as pd
import re
import pickle
import os


def load_data(path, cols, usecols=None):
    df = pd.read_csv(path, sep='\t', header=None, names=cols, usecols=usecols)
    return df


def merge_data(master, mapping_df):
    master['subject'] = master['subject'].str.replace('www.freebase.com/m/', '')
    master['object'] = master['object'].str.replace('www.freebase.com/m/', '')
    master = master.merge(mapping_df[['mid', 'description']], left_on='subject', right_on='mid')
    return master


def replace_subject_mappings(subject):
    replaced_subject = re.sub(r'<f_m.', '', subject)
    replaced_subject = re.sub(r'>', '', replaced_subject)
    return replaced_subject


def delete_data(data):
    del data


def create_column(df, params, colnames):
    for param, col in zip(params, colnames):
        df[col] = param
    return df


def unpickle_data(path, filename):
    with open(os.path.join(path, filename), 'rb') as f:
        data = pickle.load(f)
    return data


def pickle_obj(obj, filename):
    with open(f'{filename}.pkl', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

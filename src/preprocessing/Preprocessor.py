import pandas as pd
import numpy as np
import sys
import re
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical
import nltk
from sklearn.preprocessing import LabelEncoder
from sklearn.pipeline import TransformerMixin
from sklearn.base import BaseEstimator

np.random.seed(1)
nltk.download('stopwords')
stopwords = nltk.corpus.stopwords.words("english")


# sys.path.append('/glove')
def clean_text(texts, remove_list=None):
    texts = texts.str.lower()
    if remove_list is None:
        remove_list = ["@en", "?", ",", "(", ")", ":", '.', "'s"]
    for remove in remove_list:
        if remove != "'s":
            texts = texts.str.replace(remove, '')
        else:
            texts.str.replace(remove, " s")
    return texts


def relation_fit(relation_list):
    relation = pd.concat(relation_list, ignore_index=True)
    encoder = LabelEncoder()
    encoder.fit(list(set(relation)))
    return encoder


def relation_transform(rel, encoder):
    relation_arr = encoder.transform(rel).astype(np.int32).reshape(-1, 1)
    Y = to_categorical(relation_arr, 1837)
    return Y


class Sequencer(Tokenizer, BaseEstimator, TransformerMixin):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def fit(self, texts, y=None):
        self.fit_on_texts(texts)
        return self

    def transform(self, texts, y=None):
        return np.array(self.texts_to_sequences(texts))


class Padder(BaseEstimator, TransformerMixin):

    def __init__(self, maxlen=34, padding='post'):
        self.maxlen = maxlen
        self.max_index = None
        self.padding = padding

    def fit(self, X, y=None):
        self.max_index = pad_sequences(X, maxlen=self.maxlen, padding=self.padding).max()
        return self

    def transform(self, X, y=None):
        X = pad_sequences(X, maxlen=self.maxlen, padding=self.padding)
        return X


class EntityPreprocessor:
    def __init__(self,
                 #                filter_list='!"#$%&()*+,./:;<=>?...',
                 max_len=34):
        self.max_len = max_len

    def create_targets(self, df):
        targets = []
        for sel, text in zip(df['description'], df['question']):
            sel = sel.split()
            text = text.split()
            arr1 = []
            for s in sel:
                arr2 = []
                for t in text:
                    if s == t:
                        arr2.append(1)
                    else:
                        arr2.append(0)
                arr1.append(arr2)
            targets.append(np.sum(arr1, axis=0).tolist())
        return targets

    def zerolistmaker(self, l):
        length = len(l)
        listofzeros = [0] * (self.max_len - length)
        return l.extend(listofzeros)


import sys
import pandas as pd
import numpy as np
from src.preprocessing import Preprocessor
from src.utils import util_functions as futils
from src.relation_detection import modelling
from src.entity_detection import entity_detection
from src.training import training
from src.indexing import create_index
from src.entity_linking import entity_linking
from src.answer_evaluation import answer_evaluation
import time

sys.path.append('../data')
sys.path.append('../models')

train = futils.load_data('../data/annotated_fb_data_train.txt', ['subject', 'relation', 'object', 'question'])
test = futils.load_data('../data/annotated_fb_data_test.txt', ['subject', 'relation', 'object', 'question'])
validation = futils.load_data('../data/annotated_fb_data_valid.txt', ['subject', 'relation', 'object', 'question'])
mappings = futils.load_data('../data/freebase-rs-label-en.dms', ['mid', 'description'], [0, 2])

mappings['mid'] = mappings['mid'].apply(lambda x: futils.replace_subject_mappings(x))

print('data upload done')

train = futils.merge_data(train, mappings)
test = futils.merge_data(test, mappings)
validation = futils.merge_data(validation, mappings)
futils.delete_data(mappings)

train['description'] = Preprocessor.clean_text(train['description'])
test['description'] = Preprocessor.clean_text(test['description'])
validation['description'] = Preprocessor.clean_text(validation['description'])

train['question'] = Preprocessor.clean_text(train['question'])
test['question'] = Preprocessor.clean_text(test['question'])
validation['question'] = Preprocessor.clean_text(validation['question'])

sequencer = Preprocessor.Sequencer(oov_token='<OOV>', filters='!"#$%&()+,./*:;<=>?...')
sequencer.fit(pd.concat([train['question'], validation['question'], test['question']], ignore_index=True))
word_index = sequencer.word_index

train_sequences = sequencer.transform(train['question'])
valid_sequences = sequencer.transform(validation['question'])
test_sequences = sequencer.transform(test['question'])

padder = Preprocessor.Padder()
X_train = padder.transform(train_sequences)
X_valid = padder.transform(valid_sequences)
X_test = padder.transform(test_sequences)

train_relation = train.relation
valid_relation = validation.relation
test_relation = test.relation
relation_list = [test_relation, valid_relation, train_relation]

encoder = Preprocessor.relation_fit(relation_list)

entities = list(np.concatenate([train[['subject', 'description']].drop_duplicates().description.values,
                                validation[['subject', 'description']].drop_duplicates().description.values,
                                test[['subject', 'description']].drop_duplicates().description.values]))

entities_mid = list(np.concatenate([train[['subject', 'description']].drop_duplicates().subject.values,
                                    validation[['subject', 'description']].drop_duplicates().subject.values,
                                    test[['subject', 'description']].drop_duplicates().subject.values]))

inverse_index = create_index.create_inverse_index(entities, entities_mid)
reach_index = create_index.create_reach_index([train, validation, test])
main_index = create_index.create_main_index([train, validation, test], cols=['subject',
                                                                             'description',
                                                                             'relation'])


create_index.post_to_es(index=main_index, index_name='main_index', field_limits=1000)

rel_list = ['rel_model_LSTM_1_50', 'rel_model_GRU_1_50', 'rel_model_GRU_1_0']
ent_list = ['ent_model_GRU_1_50', 'ent_model_GRU_1_25', 'ent_model_GRU_1_0']

print('Prediction is starting')
for rel_model in rel_list[:1]:
    for ent_model in ent_list[:1]:
        relation_detector = training.read_model(f'../models/{rel_model}.h5')
        entity_detector = training.read_model(f'../models/{ent_model}.h5')

        relation_predictions = modelling.predict_model(relation_detector, X_valid, encoder)
        entity_predictions = entity_detection.predict_model(entity_detector, X_valid, word_index, validation.question,
                                                            cutoff=0.5)

        validation = futils.create_column(df=validation,
                                     params=[entity_predictions, relation_predictions],
                                     colnames=['entity_predictions', 'relation_predictions'])

        ent_preds, rel_preds = entity_linking.get_predictions(validation, reach_index, inverse_index)

        # create predictions for entities and relations based on TFIDF (BM25) via Elasticsearch
        rel_preds_2 = []
        ent_preds_2 = []
        for ent, rel in zip(entity_predictions, relation_predictions):
            rel_preds_2.append(entity_linking.get_results(rel, ent)['relation'])
            ent_preds_2.append(entity_linking.get_results(rel, ent)['subject'])
            time.sleep(0.005)
        ents = [ent_preds, ent_preds_2]
        rels = [rel_preds, rel_preds_2]
        scoring = ['levensthein', 'BM25']

        for ent, rel, score in zip(ents, rels, scoring):
            answer_evaluation.evaluate_answers(subjects=ent, relations=rel, eval_df=validation, scoring=score,
                                               outfile=f"{rel_model}_{ent_model}", dataset='validation')

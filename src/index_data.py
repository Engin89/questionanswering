import sys
from src.preprocessing import Preprocessor
from src.utils import util_functions as futils
import pandas as pd
from src.indexing import create_index
import spacy
nlp = spacy.load("en_core_web_sm")

sys.path.append('../data')
sys.path.append('../models')

train = futils.load_data('../data/annotated_fb_data_train.txt', ['subject', 'relation', 'object', 'question'])
test = futils.load_data('../data/annotated_fb_data_test.txt', ['subject', 'relation', 'object', 'question'])
validation = futils.load_data('../data/annotated_fb_data_valid.txt', ['subject', 'relation', 'object', 'question'])
mappings = futils.load_data('../data/freebase-rs-label-en.dms', ['mid', 'description'], [0, 2])

mappings['mid'] = mappings['mid'].apply(lambda x: futils.replace_subject_mappings(x))

print('data upload done')

train = futils.merge_data(train, mappings)
test = futils.merge_data(test, mappings)
validation = futils.merge_data(validation, mappings)

mappings = mappings.rename(columns = {'description': 'obj_descr'})
test = test.merge(mappings, left_on = 'object', right_on= 'mid', how = 'left', copy=False)
train = train.merge(mappings, left_on = 'object', right_on= 'mid', how = 'left', copy=False)
validation = validation.merge(mappings, left_on = 'object', right_on= 'mid', how = 'left', copy=False)

test = test.groupby(['subject', 'relation', 'object', 'question','description'])['obj_descr'].max().reset_index()
train = train.groupby(['subject', 'relation', 'object', 'question','description'])['obj_descr'].max().reset_index()
validation = validation.groupby(['subject', 'relation', 'object', 'question','description'])['obj_descr'].max().reset_index()
futils.delete_data(mappings)

test = test.groupby(['subject', 'relation', 'object', 'question','description'])['obj_descr'].max().reset_index()
train = train.groupby(['subject', 'relation', 'object', 'question','description'])['obj_descr'].max().reset_index()
validation = validation.groupby(['subject', 'relation', 'object', 'question','description'])['obj_descr'].max().reset_index()

train['description'] = Preprocessor.clean_text(train['description'])
test['description'] = Preprocessor.clean_text(test['description'])
validation['description'] = Preprocessor.clean_text(validation['description'])

train['obj_descr'] = Preprocessor.clean_text(train['obj_descr'])
test['obj_descr'] = Preprocessor.clean_text(test['obj_descr'])
validation['obj_descr'] = Preprocessor.clean_text(validation['obj_descr'])

train['question'] = Preprocessor.clean_text(train['question'])
test['question'] = Preprocessor.clean_text(test['question'])
validation['question'] = Preprocessor.clean_text(validation['question'])

question_df = pd.concat([train,test, validation], axis = 0)[['question', 'obj_descr']].drop_duplicates().reset_index()
answer_df = pd.concat([train,test, validation], axis = 0)[['description', 'relation', 'obj_descr']].drop_duplicates()


ner_list = []
for index, rows in question_df.iterrows():
    doc = nlp(str(rows['obj_descr']))
    if len(doc.ents) > 0:
        for entity in doc.ents:
            ner_list.append((rows['question'], rows['obj_descr'], entity.label_))
    else:
        ner_list.append((rows['question'], rows['obj_descr'], 'NO NER'))

q_df = pd.DataFrame(ner_list, columns=['question', 'obj_descr', 'label'])

question_index = q_df.to_json(orient= 'records', force_ascii = False)
answer_index = answer_df.to_json(orient= 'records', force_ascii = False)

create_index.post_to_es(index=question_index, index_name='question_index', field_limits=1000)
create_index.post_to_es(index=answer_index, index_name='answer_index', field_limits=1000)
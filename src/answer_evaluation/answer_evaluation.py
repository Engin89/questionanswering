import pandas as pd


def evaluate_answers(subjects, relations, eval_df, scoring, outfile, dataset):
    predictions_df = pd.DataFrame(zip(subjects, relations), columns=['subject', 'relation'])
    accuracy = ((predictions_df['subject'] == eval_df['subject']) & (
                predictions_df['relation'] == eval_df['relation'])).sum() / eval_df.shape[0]
    print("accuracy with {0}: {1:.4f}".format(scoring, accuracy))
    with open(f'{scoring}_{outfile}.txt', "wt") as f:
        f.write("accuracy with {0}: {1:.4f} on {2}".format(scoring, accuracy, dataset))


# !/bin/bash

import sys
import pandas as pd
from src.preprocessing import Preprocessor
from src.utils import util_functions as futils
from src.relation_detection import modelling
from src.entity_detection import entity_detection
from src.training import training
from src.entity_linking import entity_linking
import random
import time
import requests
from flask import Flask, render_template, url_for, request, redirect

sys.path.append('/data')
sys.path.append('/models')
sys.path.append('/objects')

sequencer = futils.unpickle_data(path='objects', filename='sequencer.pkl')
padder = futils.unpickle_data(path='objects', filename='padder.pkl')
encoder = futils.unpickle_data(path='objects', filename='encoder.pkl')
rel_model = 'rel_model_LSTM_1_50'
ent_model = 'ent_model_GRU_1_50'
relation_detector = training.read_model(f'models/{rel_model}.h5')
entity_detector = training.read_model(f'models/{ent_model}.h5')
word_index = sequencer.word_index
questions_list = []
answers_list = []
ground_truth_list = []
ai_answer_list = []
ai_choice_list = []

for i in range(5):
    correct_question = entity_linking.get_question()
    question = correct_question[0]['_source']['question']
    category = correct_question[0]['_source']['label']
    obj = correct_question[0]['_source']['obj_descr']

    questions = entity_linking.get_other_options(category=category, obj_descr=obj)
    questions.extend(correct_question)
    questions_list.append(question)

    answers = [q['_source']['obj_descr'] for q in questions]
    answers = random.sample(answers, 4)
    #answers_list.append(answers)
    
    #Get the correct answer from Elasticsearch
    ground_truth = entity_linking.get_ground_truth(question, category=category)
    ground_truth_list.append(ground_truth)

    #AI answer preprocessing
    question = Preprocessor.clean_text(pd.Series([question]))
    sequence = sequencer.transform(question)
    sequence = padder.transform(sequence)

    #AI answer relation prediction
    relation_prediction = modelling.predict_model(relation_detector, sequence, encoder)

    #AI answer entity detection
    entity_prediction = entity_detection.predict_model(entity_detector, sequence, word_index, question, cutoff=0.5)
    #AI answer etraction from Knowledge Graph
    ai_answer = entity_linking.get_answers(relation_prediction[0], entity_prediction[0])['obj_descr']
    if ai_answer == "NotFound":
        ai_answer = ground_truth
    ai_answer_list.append(ai_answer)
    #Include ai_answer if it is not in the choices
    if ai_answer not in answers:
        incorrect_answers_ix = [answers.index(ans) for ans in answers if ans != ground_truth]
        answers[random.sample(incorrect_answers_ix, 1)[0]] = ai_answer
    answers_list.append(answers)

    if answers.index(ai_answer) == 0:
        ai_choice = 'A'
    elif answers.index(ai_answer) == 1:
        ai_choice = 'B'
    elif answers.index(ai_answer) == 2:
        ai_choice = 'C'
    elif answers.index(ai_answer) == 3:
        ai_choice = 'D'
    ai_choice_list.append(ai_choice)

user_answer = []
app = Flask(__name__)
app.jinja_env.filters['zip'] = zip

@app.route('/<int:index>', methods=["GET", "POST"])
def welcome(index):
    if request.method == "POST":
        print(request.form['choice'])
        user_answer.append(request.form['choice'])
        return redirect(url_for("welcome", index=index + 1))
    else:
        return render_template('questions.html',
                               question=questions_list[index],
                               zip=zip(answers_list[index], ["A", "B", "C", "D"]),
                               ai_choice = ai_choice_list[index],
                               ix=index)


if __name__ == '__main__':
    app.run(debug=True)
